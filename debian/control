Source: junit4
Section: java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Florian Weimer <fw@deneb.enyo.de>,
 Varun Hiremath <varun@debian.org>,
 Ludovic Claude <ludovic.claude@laposte.net>,
 Jakub Adam <jakub.adam@ktknet.cz>,
 Emmanuel Bourg <ebourg@apache.org>
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 default-jdk-doc,
 javahelper,
 libhamcrest-java (>= 2.2),
 libmaven-javadoc-plugin-java,
 maven-debian-helper (>= 2.2)
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/java-team/junit4.git
Vcs-Browser: https://salsa.debian.org/java-team/junit4
Homepage: http://www.junit.org

Package: junit4
Architecture: all
Depends: default-jre-headless | java5-runtime-headless,
         libhamcrest-java (>= 2.2),
         ${misc:Depends}
Description: JUnit regression test framework for Java
 JUnit is a simple framework to write repeatable tests. It is an
 instance of the xUnit architecture for unit testing frameworks.
 .
 JUnit 4 uses Java 5 features such as generics and annotations.  If
 you need compatibility with previous Java versions, you should use
 the junit package (version 3) instead.

Package: junit4-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: default-jdk-doc
Suggests: junit4
Description: JUnit regression test framework for Java - documentation
 JUnit is a simple framework to write repeatable tests. It is an
 instance of the xUnit architecture for unit testing frameworks.
 .
 JUnit 4 uses Java 5 features such as generics and annotations.  If
 you need compatibility with previous Java versions, you should use
 the junit package (version 3) instead.
 .
 This package contains Javadoc API documentation of JUnit 4.
